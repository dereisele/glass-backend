from glass import filemanager
from glass import models
from glass import db
from glass import dbmanager
from os import path
from secrets import token_urlsafe
from os import makedirs
import re

from pprint import pprint  # TODO: remove


def updateFileSource(id):
    return filemanager.updateFileSource(id)


def importFilerefAsSong(filerefid):
    ref = models.FileRef.query.filter_by(id=filerefid).first()
    filepath = ref.getPath()
    _, ext = path.splitext(filepath)
    audio = {}

    print(filepath, ext)

    if ext == ".mp3":
        import mutagen.easyid3
        audio = mutagen.easyid3.EasyID3(filepath)
        artist = audio["artist"][0]
        number = int(audio["tracknumber"][0].split("/")[0])
    elif ext == ".flac":
        import mutagen.flac
        audio = mutagen.flac.FLAC(filepath)
        artist = (audio.get("artists") or audio["artist"])[0]
        number = int(audio["tracknumber"][0])
    else:
        return False, "type not supported"

    artist = splitArtist(artist)[0]
    title = audio["title"][0]
    album = audio["album"][0]
    album_artist = audio.get("albumartist", [artist])[0]  # TODO

    pprint(audio)

    pprint((title, number, artist, album, album_artist))

    ses = db.session()

    artist_obj, _ = dbmanager.get_or_create(ses, models.Artist, name=artist)

    album_artist_obj, _ = dbmanager.get_or_create(ses, models.Artist, name=album_artist)

    album_obj, _ = dbmanager.get_or_create(ses, models.Album, name=album, album_artist=album_artist_obj)

    song_obj, _ = dbmanager.get_or_create(ses, models.Song, name=title, artist=artist_obj, album=album_obj, number=number)

    filemanager.linkFileReftoSong(ref, song_obj)

    ses.close()

    return True, "ok"


def splitArtist(artistsString: str) -> ():

    delimiters = ["Feat. ", "feat. ", ", ", "And ", "and "]

    regexPattern = '|'.join(map(re.escape, delimiters))

    artists = re.split(regexPattern, artistsString)
    pprint(artists)
    if isinstance(artists, str):
        artists = [artists]
    return artists



def upload_and_enc_fileref(filerefid, target):
    ses = db.session()

    ref = models.FileRef.query.filter_by(id=filerefid).first()
    up_target = models.FileSource.query.filter_by(name=target, source_type="upload").first()
    target = models.FileSource.query.filter(models.FileSource.name == target
                                            and models.FileSource.source_type != "upload").first()

    # Generates random 32 char long base64 string as key
    key = token_urlsafe(24)
    print(key, len(key))

    encref = models.EncFileRef(filesource=target, path=ref.path+".enc", key=key.encode("utf-8"))

    ses.add(encref)
    ses.commit()

    targetdir = path.dirname(encref.getPath())

    if not path.exists(targetdir):
        makedirs(targetdir)

    filemanager.encryptFile(ref.getPath(), encref.getPath(), key)

    ses.close()
    return True, "ok"
