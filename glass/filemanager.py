import os
from urllib.parse import quote_plus
from pprint import pprint  # TODO: remove
import subprocess

from glass import models
from glass import db
from glass import dbmanager


def getInternalFileURL(root_url, fileref: models.FileRef):
    """Get the URL of a local file. Streaming is handled by glass itself."""
    return os.path.join(root_url, "dl", "files", str(fileref.id), quote_plus(os.path.basename(fileref.path)))


def linkFileReftoSong(fileref: models.FileRef, song: models.Song):
    ses = db.session()
    dbmanager.get_or_create(ses, models.SongFileRef, file_ref=fileref, song=song)
    ses.close()


def linkFileReftoSongviaID(filerefid, songid):
    ses = db.session()
    dbmanager.get_or_create(ses, models.SongFileRef, file_ref_id=filerefid, song_id=songid)
    ses.close()


def linkEncFileReftoSong(encfileref: models.EncFileRef, song: models.Song):
    ses = db.session()
    dbmanager.get_or_create(ses, models.SongFileRef, enc_file_ref=encfileref, song=song)
    ses.close()


def linkEncFileReftoSongviaID(encfilerefid, songid):
    ses = db.session()
    dbmanager.get_or_create(ses, models.SongEncFileRef, enc_file_ref_id=encfilerefid, song_id=songid)
    ses.close()


def updateFileSource(id):
    fs: models.FileSource = models.FileSource.query.filter_by(id=id).first()
    if not fs:
        return False, "filesource not found"
    if fs.source_type not in ["import", "local"]:
        return False, "wrong filesource type"

    ses = db.session()

    for root, dir, files in os.walk(fs.base_url, followlinks=True):
        pprint(files)
        for file in files:
            path = os.path.join(root, file)
            if os.path.isfile(path):
                pprint(path)
                relativepath = os.path.relpath(path, fs.base_url)
                pprint(relativepath)
                dbmanager.get_or_create(ses, models.FileRef, filesource=fs, path=relativepath)

    ses.close()

    return True, "ok"


def encryptFile(source, target, key):
    cmd = ["openssl", "aes-256-cbc", "-in", source, "-out", target, "-k", key]
    pprint(cmd)
    p = subprocess.Popen(cmd)
