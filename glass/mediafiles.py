from glass import app
from glass import models

from flask import send_file


@app.route("/dl/files/<int:ref_id>/<string:name>")
def fileref(ref_id, name):
    """Download a file. "name" is just for a nice url for the user."""
    file_ref = models.FileRef.query.filter_by(id=ref_id).first()
    print(file_ref.getPath())
    return send_file(file_ref.getPath())
