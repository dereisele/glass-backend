from os import path

from glass import db


# TV SHOWS

class Show(db.Model):
    __tablename__ = "shows"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    year = db.Column(db.Integer, nullable=True)
    lang = db.Column(db.String(8))


class Scraper(db.Model):
    __tablename__ = "scrapers"
    id = db.Column(db.Integer, primary_key=True)
    string_id = db.Column(db.String(32))


class ShowScraperRef(db.Model):
    __tablename__ = "show_scraper_ref"
    id = db.Column(db.Integer, primary_key=True)
    show_id = db.Column(db.Integer, db.ForeignKey("shows.id"))
    scraper_id = db.Column(db.Integer, db.ForeignKey("scrapers.id"))
    scraper = db.relationship("Scraper")
    url = db.Column(db.String(96))
    x = db.Column(db.String(64))


class Season(db.Model):
    __tablename__ = "seasons"
    id = db.Column(db.Integer, primary_key=True)
    show_id = db.Column(db.Integer, db.ForeignKey("shows.id"))
    show = db.relationship("Show", backref="seasons")
    number = db.Column(db.Integer)
    name = db.Column(db.String(32))


class Episode(db.Model):
    __tablename__ = "episodes"
    id = db.Column(db.Integer, primary_key=True)
    season_id = db.Column(db.Integer, db.ForeignKey("seasons.id"))
    season = db.relationship("Season",  backref="episodes")
    number = db.Column(db.Integer)
    name = db.Column(db.String(32))
    x = db.Column(db.String(64))


class EpisodeScraperRef(db.Model):
    __tablename__ = "episode_scraper_ref"
    id = db.Column(db.Integer, primary_key=True)
    episode_id = db.Column(db.Integer, db.ForeignKey("episodes.id"))
    scraper_id = db.Column(db.Integer, db.ForeignKey("scrapers.id"))
    scraper = db.relationship("Scraper")
    url = db.Column(db.String(96))
    x = db.Column(db.String(64))


# MUSIC

class Artist(db.Model):
    __tablename__ = "artists"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))


class Album(db.Model):
    __tablename__ = "albums"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    album_artist_id = db.Column(db.Integer, db.ForeignKey("artists.id"))
    album_artist = db.relationship("Artist", backref="albums")


# Keep it simple for now....
class Song(db.Model):
    __tablename__ = "songs"
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer)
    name = db.Column(db.String(64))
    artist_id = db.Column(db.Integer, db.ForeignKey("artists.id"))
    artist = db.relationship("Artist", backref="songs")
    album_id = db.Column(db.Integer, db.ForeignKey("albums.id"))
    album = db.relationship("Album", backref="songs")


class SongFileRef(db.Model):
    __tablename__ = "song_file_ref"
    id = db.Column(db.Integer, primary_key=True)
    song_id = db.Column(db.Integer, db.ForeignKey("songs.id"))
    song = db.relationship("Song", backref="file_refs")
    file_ref_id = db.Column(db.Integer, db.ForeignKey("file_ref.id"))
    file_ref = db.relationship("FileRef")


class SongEncFileRef(db.Model):
    __tablename__ = "song_enc_file_ref"
    id = db.Column(db.Integer, primary_key=True)
    song_id = db.Column(db.Integer, db.ForeignKey("songs.id"))
    song = db.relationship("Song", backref="enc_file_refs")
    enc_file_ref_id = db.Column(db.Integer, db.ForeignKey("enc_file_ref.id"))
    enc_file_ref = db.relationship("EncFileRef")


class CoverFileRef(db.Model):
    __tablename__ = "cover_file_ref"
    id = db.Column(db.Integer, primary_key=True)
    album_id = db.Column(db.Integer, db.ForeignKey("albums.id"))
    album = db.relationship("Album", backref="cover_file_refs")
    file_ref_id = db.Column(db.Integer, db.ForeignKey("file_ref.id"))
    file_ref = db.relationship("FileRef")


# FILE STUFF

class FileSource(db.Model):
    __tablename__ = "filesources"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16))
    source_type = db.Column(db.String(6))
    base_url = db.Column(db.String(64))


class FileRef(db.Model):
    __tablename__ = "file_ref"
    id = db.Column(db.Integer, primary_key=True)
    filesource_id = db.Column(db.Integer, db.ForeignKey("filesources.id"))
    filesource = db.relationship("FileSource", backref="filerefs")
    path = db.Column(db.String(128))

    def getPath(self):
        return path.join(self.filesource.base_url, self.path)


class EncFileRef(db.Model):
    __tablename__ = "enc_file_ref"
    id = db.Column(db.Integer, primary_key=True)
    filesource_id = db.Column(db.Integer, db.ForeignKey("filesources.id"))
    filesource = db.relationship("FileSource", backref="encfilerefs")
    path = db.Column(db.String(128))
    key = db.Column(db.LargeBinary(32))

    def getPath(self):
        return path.join(self.filesource.base_url, self.path)
