from flask import jsonify
from flask import request
from flask import send_from_directory

from glass import app
from glass import controller
from glass import filemanager
from glass import models


# TV SHOWS

@app.route("/shows")
def shows():
    limit = int(request.args.get("limit", 10))
    offset = int(request.args.get("offset", 0))
    shows = models.Show.query.offset(offset).limit(limit).all()
    result = []
    for s in shows:
        result.append({"name": s.name,
                       "lang": s.lang,
                       "year": s.year,
                       "id":   s.id,})

    return jsonify({"status": 200,
                    "result": result,
                    "limit":  limit,
                    "offset": offset,
                    "count":  len(shows)})


@app.route("/shows/<int:show_id>")
def show(show_id):
    s = models.Show.query.filter_by(id=show_id).first()

    if not s:
        return jsonify({"status": 404, "result": None}), 404

    rs_seasons = []
    for se in s.seasons:
        rs_seasons.append({"number": se.number,
                           "name":   se.name or "Season " + str(se.number),
                           "id":     se.id,})

    result = {"name": s.name,
              "lang": s.lang,
              "year": s.year,
              "id":   s.id,
              "seasons": rs_seasons}
    return jsonify({"status": 200, "result": result})


@app.route("/seasons/<int:id>")
def season_by_id(id):
    s = models.Season.query.filter_by(id=id).first()

    if s == None:
        return jsonify({"status": 404, "result": None}), 404

    result = {"name": s.name,
              "id": s.id,
              "number": s.number,
              "show": {"name": s.show.name,
                       "id":   s.show_id},}

    return jsonify({"status": 200, "result": result})


@app.route("/episodes/<int:id>")
def episode_by_id(id):
    e = models.Episode.query.filter_by(id=id).first()

    if not e:
        return jsonify({"status": 404, "result": None}), 404

    result = {"name": e.name,
              "id": e.id,
              "number": e.number,
              "season": {"name": (e.season.name or "Season " + str(e.season.number)),
                         "number": e.season.number,
                         "id":   e.season_id},
              "show": {"name": e.season.show.name,
                       "id": e.season.show.id}}

    return jsonify({"status": 200, "result": result})


@app.route("/shows/<int:show_id>/seasons/<int:season_number>")
def season_by_show(show_id, season_number):
    s = models.Season.query.filter_by(show_id=show_id, number=season_number).first()

    if not s:
        return jsonify({"status": 404, "result": None}), 404

    result = {"name": (s.name or "Season " + str(s.number)),
              "number": s.number,
              "id": s.id,
              "show": {"name": s.show.name,
                       "id":   s.show_id},
              "episodes": [{"number": e.number,
                            "name": e.name,
                            "id": e.id} for e in s.episodes]}  # This is why Python is awesome!

    return jsonify({"status": 200, "result": result})


@app.route("/shows/<int:show_id>/seasons/<int:season_number>/episodes/<int:episode_number>")
def episode_by_show(show_id, season_number, episode_number):
    e = models.Episode.query.join(models.Episode.season).filter((models.Episode.number == episode_number)
                                                                and (models.Episode.season.number == season_number)
                                                                and (models.Episode.season.show_id == show_id)).first()

    if not e:
        return jsonify({"status": 404, "result": None}), 404

    result = {"name": (e.name or "Episode " + str(e.number)),
              "number": e.number,
              "id": e.id,
              "season": {"name"  : (e.season.name or "Season " + str(e.season.number)),
                         "number": e.season.number,
                         "id"    : e.season_id},
              "show"  : {"name": e.season.show.name,
                         "id"  : e.season.show.id}
              }

    return jsonify({"status": 200, "result": result})


# MUSIC

@app.route("/artists")
def artists():
    limit = int(request.args.get("limit", 10))
    offset = int(request.args.get("offset", 0))
    artists = models.Artist.query.order_by(models.Artist.name).offset(offset).limit(limit).all()
    result = []
    for a in artists:
        result.append({"name": a.name,
                       "id":   a.id})

    return jsonify({"status": 200,
                    "result": result,
                    "limit":  limit,
                    "offset": offset,
                    "count":  len(artists)})


@app.route("/artists/<int:artist_id>")
def artist(artist_id):
    a = models.Artist.query.filter_by(id=artist_id).first()

    if not a:
        return jsonify({"status": 404, "result": None}), 404

    rs_albums = []
    for al in a.albums:
        rs_albums.append({"name":   al.name,
                          "id":     al.id})

    result = {"name": a.name,
              "id":   a.id,
              "albums": rs_albums}
    return jsonify({"status": 200, "result": result})


@app.route("/artists/<int:artist_id>/albums")
def albums_by_artist(artist_id):
    limit = int(request.args.get("limit", 10))
    offset = int(request.args.get("offset", 0))
    albums = models.Album.query.filter_by(album_artist_id=artist_id).offset(offset).limit(limit).all()
    result = []
    for a in albums:
        if a.cover_file_refs:
            cover = filemanager.getInternalFileURL(request.host_url, a.cover_file_refs[0].file_ref)
        else:
            cover = None
        result.append({"name": a.name,
                       "id":   a.id,
                       "cover": cover})

    return jsonify({"status": 200,
                    "result": result,
                    "limit":  limit,
                    "offset": offset,
                    "count":  len(albums)})


@app.route("/artists/<int:artist_id>/songs")
def songs_by_artist(artist_id):
    limit = int(request.args.get("limit", 10))
    offset = int(request.args.get("offset", 0))
    songs = models.Song.query.filter_by(artist_id=artist_id).offset(offset).limit(limit).all()
    result = []
    for s in songs:
        result.append({"name": s.name,
                       "id":   s.id,
                       "album": {"id": s.album_id,
                                 "name": s.album.name}})

    return jsonify({"status": 200,
                    "result": result,
                    "limit":  limit,
                    "offset": offset,
                    "count":  len(songs)})


@app.route("/albums")
def albums():
    limit = int(request.args.get("limit", 10))
    offset = int(request.args.get("offset", 0))
    albums = models.Album.query.offset(offset).limit(limit).all()
    result = []
    for a in albums:
        if a.cover_file_refs:
            cover = filemanager.getInternalFileURL(request.host_url, a.cover_file_refs[0].file_ref)
        else:
            cover = None

        result.append({"name": a.name,
                       "id":   a.id,
                       "cover": cover,
                       "artist": {"name": a.album_artist.name,
                                  "id":   a.album_artist.id}})

    return jsonify({"status": 200,
                    "result": result,
                    "limit":  limit,
                    "offset": offset,
                    "count":  len(albums)})


@app.route("/albums/<int:id>")
def album_by_id(id):
    a = models.Album.query.filter_by(id=id).first()

    if not a:
        return jsonify({"status": 404, "result": None}), 404

    if a.cover_file_refs:
        cover = filemanager.getInternalFileURL(request.host_url, a.cover_file_refs[0].file_ref)
    else:
        cover = None

    result = {"name": a.name,
              "id": a.id,
              "cover": cover,
              "artist": {"name": a.album_artist.name,
                         "id":   a.album_artist_id},
              "songs": [{"number": s.number,
                         "name": s.name,
                         "id": s.id,
                         "artist": {"id": s.artist.id,
                                    "name": s.artist.name}} for s in a.songs]}

    return jsonify({"status": 200, "result": result})


@app.route("/songs/<int:id>")
def song_by_id(id):
    s = models.Song.query.filter_by(id=id).first()

    if not s:
        return jsonify({"status": 404, "result": None}), 404

    files = []
    for ref in s.file_refs:
        if ref.file_ref.filesource.source_type == "local":
            files.append({
                "type": "internal",
                "path": filemanager.getInternalFileURL(request.host_url, ref.file_ref)})
        else:
            files.append({
                "type": ref.file_ref.filesource.source_type,
                "path": ref.file_ref.getPath()})

    result = {"number": s.number,
              "name": s.name,
              "id": s.id,
              "artist": {"id": s.artist.id,
                         "name": s.artist.name},
              "album": {"id": s.album_id,
                        "name": s.album.name,
                        "artist": {"id": s.album.album_artist_id,
                                   "name": s.album.album_artist.name}},
              "files": files,
              "encfiles": [{"path": ref.enc_file_ref.getPath(),
                            "key":  ref.enc_file_ref.key,
                            "type": ref.enc_file_ref.filesource.source_type} for ref in s.enc_file_refs]
              }
    return jsonify({"status": 200, "result": result})


@app.route("/albums/<int:albumid>/songs/<int:number>")
def song_by_album(albumid, number):
    s = models.Song.query.filter_by(album_id=albumid, number=number).first()

    if not s:
        return jsonify({"status": 404, "result": None}), 404

    files = []
    for ref in s.file_refs:
        if ref.file_ref.filesource.source_type == "local":
            files.append({
                "type": "internal",
                "path": filemanager.getInternalFileURL(request.host_url, ref.file_ref)})
        else:
            files.append({
                "type": ref.file_ref.filesource.source_type,
                "path": ref.file_ref.getPath()})

    result = {"number": s.number,
              "name": s.name,
              "id": s.id,
              "artist": {"id": s.artist.id,
                         "name": s.artist.name},
              "album": {"id": s.album_id,
                        "name": s.album.name,
                        "artist": {"id": s.album.album_artist_id,
                                   "name": s.album.album_artist.name}},
              "files": files,
              "encfiles": [{"path": ref.enc_file_ref.getPath(),
                            "key":  ref.enc_file_ref.key,
                            "type": ref.enc_file_ref.filesource.source_type} for ref in s.enc_file_refs]
              }
    return jsonify({"status": 200, "result": result})


@app.route("/filesources")
def filesources():
    fs = models.FileSource.query.all()

    result = [{"id": f.id,
               "name": f.name,
               "type": f.source_type,
               "base_url": f.base_url} for f in fs]
    return jsonify({"status": 200, "result": result})


@app.route("/filesources/<int:id>")
def filesource_by_id(id):
    fs = models.FileSource.query.filter_by(id=id).first()
    result = {"id": fs.id,
              "name": fs.name,
              "type": fs.source_type,
              "base_url": fs.base_url,
              "file_ref": [{"id": ref.id,
                            "path": ref.path} for ref in fs.filerefs]}
    return jsonify({"status": 200, "result": result})


@app.route("/filesources/<int:id>/update")
def updatefs(id):
    status = controller.updateFileSource(id)
    if status[0]:
        return jsonify({"status": 200, "info": status[1]})
    else:
        return jsonify({"status": 500, "info": status[1]}), 500


@app.route("/filerefs/<int:filerefid>/importSong")
def import_fileref_as_song(filerefid):
    status = controller.importFilerefAsSong(filerefid)
    if status[0]:
        return jsonify({"status": 200, "info": status[1]})
    else:
        return jsonify({"status": 500, "info": status[1]}), 500


# TODO: just for testing
@app.route("/filerefs/<int:filerefid>/encupload/<string:target>")
def upload_and_enc_fileref(filerefid, target):
    status = controller.upload_and_enc_fileref(filerefid, target)
    if status[0]:
        return jsonify({"status": 200, "info": status[1]})
    else:
        return jsonify({"status": 500, "info": status[1]}), 500

@app.route('/web/<path:path>')
def glass_web(path):
    return send_from_directory(app.config["GLASSWEB_DIR"], path)
