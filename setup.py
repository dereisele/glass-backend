from setuptools import setup

setup(
    name='glass',
    packages=['glass'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-SQLAlchemy'
    ],
)